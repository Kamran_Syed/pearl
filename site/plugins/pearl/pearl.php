<?php
/*
Plugin Name: Pearl 
Plugin URI: 
Description: Raja Jewellery
Version: 1.0
Author: AgileSolutionspk
Author URI: http://agilesolutionspk.com/
*/
add_action( 'plugins_loaded',  'agile_gateway_load' );
add_filter( 'woocommerce_payment_gateways', 'add_your_gateway_class' );
	function add_your_gateway_class( $methods ) {
		$methods[] = 'Agile_Authorize_Gateway'; 
		return $methods;
	}
	function agile_gateway_load() {
		class Agile_Authorize_Gateway extends WC_Payment_Gateway {
			
			function __construct() {
						
				// The global ID for this Payment method
				$this->id = "pearl_authorize_net";
		 
				// The Title shown on the top of the Payment Gateways Page next to all the other Payment Gateways
				$this->method_title = __( "Pearl Authorize.net", 'pearl_authorize_net' );
		 
				// The description for this Payment Gateway, shown on the actual Payment options page on the backend
				$this->method_description = __( "Authorize.net AIM Pearl Payment Gateway Plug-in for WooCommerce", 'pearl_authorize_net' );
		 
				// The title to be used for the vertical tabs that can be ordered top to bottom
				$this->title = __( "Pearl Authorize.net", 'pearl_authorize_net' );
				
		 
				// If you want to show an image next to the gateway's name on the frontend, enter a URL to an image.
				$this->icon = null;
		 
				// Bool. Can be set to true if you want payment fields to show on the checkout 
				// if doing a direct integration, which we are doing in this case
				$this->has_fields = true;
		 
				// Supports the default credit card form
				$this->supports = array( 'default_credit_card_form' );
		 
				// This basically defines your settings which are then loaded with init_settings()
				$this->init_form_fields();
				
				// Save settings
				if ( is_admin() ) {
					// Versions over 2.0
					// Save our administration options. Since we are not going to be doing anything special
					// we have not defined 'process_admin_options' in this class so the method in the parent
					// class will be used instead
					add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
				}       
			} // End __construct()
			
			// Build the administration fields for this specific Gateway
			public function init_form_fields() {
				$this->form_fields = array(
					'enabled' => array(
						'title'     => __( 'Enable / Disable', 'spyr-authorizenet-aim' ),
						'label'     => __( 'Enable this payment gateway', 'spyr-authorizenet-aim' ),
						'type'      => 'checkbox',
						'default'   => 'no',
					),
					'title' => array(
						'title'     => __( 'Title', 'spyr-authorizenet-aim' ),
						'type'      => 'text',
						'desc_tip'  => __( 'Payment title the customer will see during the checkout process.', 'spyr-authorizenet-aim' ),
						'default'   => __( 'Credit card', 'spyr-authorizenet-aim' ),
					),
					'description' => array(
						'title'     => __( 'Description', 'spyr-authorizenet-aim' ),
						'type'      => 'textarea',
						'desc_tip'  => __( 'Payment description the customer will see during the checkout process.', 'spyr-authorizenet-aim' ),
						'default'   => __( 'Pay securely using your credit card.', 'spyr-authorizenet-aim' ),
						'css'       => 'max-width:350px;'
					)
				);      
			}
			
				// Submit payment and handle response
			public function process_payment( $order_id ) {
				global $woocommerce;
				
				$order = new WC_Order( $order_id );

				// Mark as on-hold (we're awaiting the cheque)
				$order->update_status('on-hold', __( 'Awaiting cheque payment', 'woocommerce' ));

				// Reduce stock levels
				$order->reduce_order_stock();

				// Remove cart
				$woocommerce->cart->empty_cart();

				// Return thankyou redirect
				return array(
					'result' => 'success',
					'redirect' => $this->get_return_url( $order )
				);
			}
		}
	}
	
if ( !class_exists( 'Agile_Preal_Raja_Jewellery' )){  
	class Agile_Preal_Raja_Jewellery{
		function __construct() {
			register_activation_hook( __FILE__, array($this, 'install') );	
		}
		
		function install(){
			
		}
	}//class ends
}//existing class ends
$aspk_raja = new Agile_Preal_Raja_Jewellery();