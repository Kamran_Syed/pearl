<?php
/*
Plugin Name: Self Authorize.Net
Plugin URI: 
Description: Payment Via Authorize.Net
Version: 1.0
Author: AgileSolutionspk
Author URI: http://agilesolutionspk.com/
*/
if ( !class_exists( 'Agile_Project_Authorize_Net_application' )){  
	class Agile_Project_Authorize_Net_application{
		function __construct() {
			register_activation_hook( __FILE__, array($this, 'install') );
			add_action( 'admin_menu', array(&$this, 'addAdminPage') );
			add_shortcode( 'aspk_authorize_net', array(&$this, 'authorize_net_form') );
			add_action('wp_enqueue_scripts', array(&$this, 'front_scripts') );
		}
		function front_scripts(){
			//wp_enqueue_script('js-jquery_graph_front',plugins_url('js/jquery_graph.js', __FILE__));
		}
		function install(){
			
		}
		
		function authorize_net_form($attr){
			global $Message , $Amount;
		    if(isset($_POST['s_submit'])){
		      	$first_name = $_POST['F_name'];
				$last_name = $_POST['L_name'];
				$email = $_POST['E_mail'];
				$state = $_POST['S_tate'];
				$zipcode = $_POST['Zip_code'];
				$city = $_POST['City'];
				$address = $_POST['Add_ress'];
				$credit_card = $_POST['creditcard_auth'];
				$expire_month = $_POST['expiremonth_auth'];	
				$expire_year  = $_POST['expireyear_auth'];
				$security_code = $_POST['secure'];
				$totalamount = $_POST['T_Amount'];
				$country = $_POST['country'];
				$this->agile_gw_payment($credit_card,$expire_month,$expire_year,$first_name,$last_name,$address,$city,$state,$zipcode,$email,$totalamount,$country);
				$page = get_page_by_title( $attr['title']);
				wp_redirect($page->guid);
				exit;
				
			}	
			
			?>
			<div class = "tw-bs container">
				<form method="post" action="">
					<!--gateway field -->
					<div style="clear:left;" class = "row">
						<div style="background-color:#CFDCE1; clear:left;padding:5px;margin-top:1em;" class = "col-md-6">
							<div style="clear:left;" class = "row">
								<div class = "col-md-12">Enter Credit Card Number</div>	
							</div>
							<div style="clear:left;" class = "row">
								<div class = "col-md-12">
									<input id="aspk_ccnumber" class = "form-control" type="text" name="creditcard_auth" value="" required id ="cc_number" />
								</div>
							</div>
						<div style="clear:left;" class = "row">
							<div class = "col-md-6">Expiration Date</div>	
							<div class = "col-md-6">Card Security Code</div>
						</div>
						<div style="clear:left;" class = "row">
							<div class = "col-md-6">
								<select  id="aspk_expdate" required type="text" name="expiremonth_auth" >
									<?php for ($x=01; $x<=12; $x++){ ?>
										<option value="<?php echo $x; ?>"><?php echo $x; ?></option>
									<?php } ?>
								</select>
								<?php 
								$newtime =  new datetime();
								$year = $newtime->format('Y');
								$upyear= $year+12;
								?>
								<select id="aspk_exp_date" required type="text" name="expireyear_auth" >
									<?php for ($x=$year; $x<=$upyear; $x++){ ?>
										<option value="<?php echo $x; ?>"><?php echo $x; ?></option>
									<?php } ?>
								</select>
							</div>	
							<div class = "col-md-6">
								<input size = "3" type="text" name="secure" value="" required id ="ask_card_code" />
							</div>
						</div>
						</div>
					</div><!--end show-div-->
					<!--gateway field -->
						<input  type = "hidden" id = "fname" name = "F_name" value ="" />
						<input  type = "hidden" id = "lname" name = "L_name" value ="" />
						<input  type = "hidden" id = "email" name = "E_mail" value ="" />
						<input  type = "hidden" id = "zip" name = "Zip_code" value ="" />
						<input  type = "hidden" id="register_countries" value ="" name="country" />
						<input  type = "hidden" id = "statereg" value ="" name = "S_tate" />
					  <input  type = "hidden" id="address" name = "Add_ress" value =""  />
						<input  type = "hidden" name = "City" id = "city"  value ="" />
						<input type = "hidden" id="t_amount" name = "T_Amount" value =""  />
					<div class = "row" style= "clear:left; margin-top: 1em;">
						<div class = "col-md-2"> 
							<input class = "form-control" id ="btn-authnet-proceed" type="submit" name="s_submit" value="Proceed"/>
						</div>
					</div>		
				<!--</form>--> 
			</div>
			   
			
			<?
 		}
		
		function addAdminPage(){
			add_options_page('Authorize.Net','Authorize.Net', 'manage_options', 'agile_gateway_key', array(&$this,'payment_transaction_key'));
		}
		
		function payment_transaction_key(){
			if(isset($_POST['api_keys'])){
				$mode = $_POST['agile_mode'];
				update_option( 'agile_authorize_mode', $mode );
				$live_api_login_id = $_POST['api_login_id'];
				$live_api_transc_id = $_POST['transaction_key'];
				$test_api_login_id = $_POST['test_api_login_id'];
				$test_api_transc_id = $_POST['test_transaction_key'];
				$x['agile_login_id'] = $live_api_login_id;
				$x['agile_transaction_key'] = $live_api_transc_id;
				$x['agile_test_login_id'] = $test_api_login_id;
				$x['agile_test_transaction_key'] = $test_api_transc_id;
				update_option( 'agile_authorize_gw_key', $x );
			}
			$get_mode = get_option('agile_authorize_mode');
			$get_val = get_option('agile_authorize_gw_key');
				if ($get_mode == 'live'){
					$checked = 'checked';
					$live_login_id = $get_val['agile_login_id'];
					$live_trans_id = $get_val['agile_transaction_key'];
				}
				if ($get_mode == 'test'){
					$checked1 = 'checked';
					$test_login_id = $get_val['agile_test_login_id'];
					$test_trans_id = $get_val['agile_test_transaction_key'];
				}
				if(isset($_POST['pay_email'])){
					$admin_email = $_POST['email'];
					update_option( 'agile_paypal_gw_email', $admin_email );
				}
				$get_email_paypal = get_option('agile_paypal_gw_email');
			?>
			<h3>Authorize.Net</h3>
					<form method = "post" action = "">
						<div style = "clear:left; float:left;">
							<div style = "float:left;" >
								<input type = "radio"  <?php if ($get_mode == 'live'){ echo $checked; }else{echo 'checked';} ?> name="agile_mode" value = "live" />
							</div>
							<div style = "float:left;" >Live Mode</div>
							<div style = "float:left;" >
								<input type = "radio" name="agile_mode" <?php  echo $checked1; ?> value = "test" />
							</div>
							<div style = "float:left;" >Test Mode</div>
						</div>
						<div style = "clear:left; float:left; width:70%;">
							<div style = "float:left; width:35%;" >API Login Id</div>
							<div style = "float:left;" >
								<Input type="text" name = "api_login_id" value = "<?php echo $live_login_id; ?>"/>
							</div>
						</div>
						<div style = "clear:left; float:left; width:70%;">
							<div style = "float:left; width:35%;" >API Transaction Key</div>
							<div style = "float:left;" >
								<Input type="text" name = "transaction_key" value = "<?php echo $live_trans_id; ?>"/>
							</div>
						</div>
						<div style = "clear:left; float:left; width:70%;">
							<div style = "float:left; width:35%;" > Test Mode API Login Id</div>
							<div style = "float:left;" >
								<Input type="text" name = "test_api_login_id" value = "<?php echo $test_login_id; ?>"/>
							</div>
						</div>
						<div style = "clear:left; float:left; width:70%;">
							<div style = "float:left; width:35%;" >Test Mode API Transaction Key</div>
							<div style = "float:left;" >
								<Input type="text" name = "test_transaction_key" value = "<?php echo $test_trans_id; ?>"/>
							</div>
						</div>
						<div style = "clear:left; float:left; width:70%;">
							<input class = "button-primary" type = "submit" name = "api_keys" value ="save" />
						</div>
					</form>
			<?php
		}
		// function authorize.net payment gateway
		function agile_gw_payment($credit_card,$expire_month,$expire_year,$f_name,$l_name,$address,$city,$state,$zipcode,$email,$totalamount,$country){
			if(!session_start()){
				session_start();
			}
			$get_mode = get_option('agile_authorize_mode');
			$get_val = get_option('agile_authorize_gw_key');
				if ($get_mode == 'live'){
					$login_id = $get_val['agile_login_id'];
					$trans_id = $get_val['agile_transaction_key'];
					$post_url = "https://secure.authorize.net/gateway/transact.dll";
				}
				if ($get_mode == 'test'){
					$login_id= $get_val['agile_test_login_id'];
					$trans_id = $get_val['agile_test_transaction_key'];
					$post_url = "https://test.authorize.net/gateway/transact.dll";
				}
			
			$post_values = array(
				
				// the API Login ID and Transaction Key must be replaced with valid values
				"x_login"			=> 	$login_id,						//"9ezBUcfB9b29",
				"x_tran_key"		=> 	$trans_id,						//"6wBJ9xVBj234em5K",

				"x_version"			=> "3.1",
				"x_delim_data"		=> "TRUE",
				"x_delim_char"		=> "|",
				"x_relay_response"	=> "FALSE",

				"x_type"			=> "AUTH_CAPTURE",
				"x_method"			=> "CC",
				"x_card_num"		=>	$credit_card ,
				"x_exp_date"		=> 	$expire_month."-".$expire_year,

				"x_amount"			=> $totalamount,
				"x_description"		=> "Sample Transaction",

				"x_first_name"		=> $f_name,
				"x_last_name"		=> $l_name,
				"x_address"			=> $address,
				"x_state"			=> $state,
				"x_country"			=> $country,
				"x_city"			=> $city,
				"x_email_customer"  => $email,
				"x_zip"				=> $zipcode
				// Additional fields can be added here as outlined in the AIM integration
				// guide at: http://developer.authorize.net
			);

			// This section takes the input fields and converts them to the proper format
			// for an http post.  For example: "x_login=username&x_tran_key=a1B2c3D4"
			$post_string = "";
			foreach( $post_values as $key => $value ){
				 $post_string .= "$key=" . urlencode( $value ) . "&"; 
			}
			$post_string = rtrim( $post_string, "& " );

			
			$request = curl_init($post_url); // initiate curl object
				curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
				curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
				curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
				curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response.
				$post_response = curl_exec($request); // execute curl post and store results in $post_response
				// additional options may be required depending upon your server configuration
				// you can find documentation on curl options at http://www.php.net/curl_setopt
			curl_close ($request); // close curl object

			$response_array = explode($post_values["x_delim_char"],$post_response);
					$message = $response_array[3];
					$txid = $response_array[6];
					$amount = $response_array[9];
					if($message == 'This transaction has been approved.'){
						$_SESSION['aspk_authnet_result'] = array('result' => 'Success', 'msg' => $message, 'transactionid'=> $txid);
					}else{
						$_SESSION['aspk_authnet_result'] = array('result' => 'Fail', 'msg' => $message, 'transactionid'=>'');
					}
			/*echo "<OL>\n";
			foreach ($response_array as $value){
				echo "<LI>" . $value . "&nbsp;</LI>\n";
			}
			echo "</OL>\n"; */
			
		} // end function authorize.net payment gateway
		
		
	}//class ends
}//existing class ends
$aspk_authorize = new Agile_Project_Authorize_Net_application();