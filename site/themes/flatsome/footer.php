<?php
/**
 * The template for displaying the footer.
 *
 * @package flatsome
 */

global $flatsome_opt;
?>

</div><!-- #main-content -->


<footer class="footer-wrapper" role="contentinfo">	
<?php if(isset($flatsome_opt['html_before_footer'])){
	// BEFORE FOOTER HTML BLOCK
	echo do_shortcode($flatsome_opt['html_before_footer']);
} ?>


<!-- FOOTER 1 -->
<?php if ( is_active_sidebar( 'sidebar-footer-1' ) ) : ?>
<div class="footer footer-1 <?php echo $flatsome_opt['footer_1_color']; ?>"  style="background-color:<?php echo $flatsome_opt['footer_1_bg_color']; ?>">
	<div class="row">
   		<?php dynamic_sidebar('sidebar-footer-1'); ?>        
	</div><!-- end row -->
</div><!-- end footer 1 -->
<?php endif; ?>


<!-- FOOTER 2 -->
<?php if ( is_active_sidebar( 'sidebar-footer-2' ) ) : ?>
<div class="footer footer-2 <?php echo $flatsome_opt['footer_2_color']; ?>" style="background-color:<?php echo $flatsome_opt['footer_2_bg_color']; ?>">
	<div class="row">

   		<?php dynamic_sidebar('sidebar-footer-2'); ?>        
	</div><!-- end row -->
</div><!-- end footer 2 -->
<?php endif; ?>


<?php if(isset($flatsome_opt['html_after_footer'])){
	// AFTER FOOTER HTML BLOCK
	echo do_shortcode($flatsome_opt['html_after_footer']);
} ?>


<div class="absolute-footer <?php echo $flatsome_opt['footer_bottom_style']; ?>" style="background-color:<?php echo $flatsome_opt['footer_bottom_color']; ?>">
	<div class="row">
		<div class="large-5 columns"></div>
		<div class="large-7 columns">
			<div style="margin:0 auto;">
				 <?php if ( has_nav_menu( 'footer' ) ) : ?>
					<?php  
							wp_nav_menu(array(
								'theme_location' => 'footer',
								'menu_class' => 'footer-nav_bar',
								'depth' => 1,
								'fallback_cb' => false,
							));
					?>						
				<?php endif; ?>
			</div><!-- .left -->
		</div><!-- .large-12 -->
	</div><!-- .row-->
	<div class="row">
		<div class="large-4 columns"></div>
		<div class="large-8 columns">
			 <?php if ( has_nav_menu( 'bottom_footer' ) ) {?>
				<?php  
						wp_nav_menu(array(
							'theme_location' => 'bottom_footer',
							'menu_class' => 'footer-nav-aspk',
						));
				?>						
			<?php } ?>
		</div>
	</div>
	<div class="row" >
		<div class="large-5 columns"><?php echo do_shortcode('[share]'); // edit this in themefolder/inc/shortcodes/share.php ?></div>
		<div class="large-7 columns"><img style="margin-left:-1.2em !important;" src="<?php echo get_bloginfo('template_url') ?>/images/raja2.jpg"/></div>
	</div>
</div><!-- .absolute-footer -->
</footer><!-- .footer-wrapper -->
</div><!-- #wrapper -->

<!-- back to top -->
<a href="#top" id="top-link"><span class="icon-angle-up"></span></a>
<div class="scroll-to-bullets"></div>

<?php if(isset($flatsome_opt['html_scripts_footer'])){
	// Insert footer scripts
	echo $flatsome_opt['html_scripts_footer'];
} ?>


<?php wp_footer(); ?>

</body>
</html>