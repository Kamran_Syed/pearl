<?php
/*
Template Name: Home Page
*/
get_header(); ?>

<div class="page-header">

</div>

<div  class="page-wrapper aspk_home_page_bc" id="content">


		<?php while ( have_posts() ) : the_post(); ?>

				<?php the_content(); ?>
				<div class="row">
					<div class="large-12 columns">
					
					</div>
				</div>
				<script>
				jQuery(window).bind('scroll mousewheel DOMMouseScroll', function(e){
					setTimeout(function(){
								if(e.originalEvent.detail > 0) {
										new Effect.Move('destination1', {x:0,y:-50,duration:3.0,from: 1.0, to: 0.9});	
										new Effect.Move('destination1', {x:0,y:50,duration:3.0});
								 }else {
										new Effect.Move('destination1', {x:0,y:-50,duration:3.0,from: 1.0, to: 0.9});
										new Effect.Move('destination1', {x:0,y:50,duration:3.0});
								 }
							 
							 return false;
					}, 1000);
				}); 
				

			</script>

		<?php endwhile; // end of the loop. ?>

</div><!-- end page-right-sidebar container -->


<?php get_footer(); ?>