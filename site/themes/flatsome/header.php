<?php
global $woo_options;
global $woocommerce;
global $flatsome_opt;
?>
<!DOCTYPE html>
<!--[if lte IE 9 ]><html class="ie lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

	<!-- Custom favicon-->
	<link rel="shortcut icon" href="<?php if ($flatsome_opt['site_favicon']) { echo $flatsome_opt['site_favicon']; ?>
	<?php } else { ?><?php echo get_template_directory_uri(); ?>/favicon.png<?php } ?>" />

	<!-- Retina/iOS favicon -->
	<link rel="apple-touch-icon-precomposed" href="<?php if ($flatsome_opt['site_favicon_large']) { echo $flatsome_opt['site_favicon_large']; ?>
	<?php } else { ?><?php echo get_template_directory_uri(); ?>/apple-touch-icon-precomposed.png<?php } ?>" />

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php 
// HTML Homepage Before Header // Set in Theme Option > HTML Blocks
if($flatsome_opt['html_intro'] && is_front_page()) echo '<div class="home-intro">'.do_shortcode($flatsome_opt['html_intro']).'</div>' ?>

	<div id="wrapper">
		<div class="header-wrapper">
		
		<?php do_action( 'before' ); ?>
		<?php if(!isset($flatsome_opt['topbar_show']) || $flatsome_opt['topbar_show']){ ?>
		<div  class="row" id="top-bar11" >
			<div class="large-12 columns">
				<div class="left-text left" style="margin-top: 8px;">
				<!--	<img src="<?php echo get_bloginfo('template_url') ?>/images/Rj-Logo.jpg"/>-->
				</div>
				<div class="right-text right" style="margin-top: 8px;">
					 <?php if ( has_nav_menu( 'top_bar_nav' ) ) { 
								wp_nav_menu(array(
									'theme_location' => 'top_bar_nav',
									'menu_class' => 'top-bar-nav',
									'before' => '',
									'after' => '',
									'link_before' => '',
									'link_after' => '',
									'depth' => 2,
									'fallback_cb' => false,
									'walker' => new FlatsomeNavDropdown 
								));
						 } ?>
				</div>
			</div>
		</div><!-- .#top-bar -->
		<?php }?>
		<div id="top-bar1" >
			<div  class="row" >
				<a href="<?php echo home_url(); ?>">
					<div class="large-12 columns">
						<img src="<?php echo get_bloginfo('template_url') ?>/images/raja.jpg"/ class="img-responsive" style="display: block;height: 118px;margin: 0 auto 0px;width: 750px;">
					</div>
				</a>
			</div>
		</div>

<div class="sticky-wrapper">
	<header id="masthead" class="site-header" role="banner">
		<div class="row"> 
			<div class="large-3 columns header-container">&nbsp;</div>
			<div class="large-9 columns header-container">
				<div class="mobile-menu show-for-small"><a href="#open-menu"><span class="icon-menu"></span></a></div><!-- end mobile menu -->
					<div class="left-links">
						<?php if(!isset($flatsome_opt['nav_position']) || $flatsome_opt['nav_position'] == 'top'){ ?>
							<ul id="site-navigation" class="header-nav">
								<?php if ( has_nav_menu( 'top_bar_nav2' ) ) : ?>

									<?php  
									wp_nav_menu(array(
										'theme_location' => 'top_bar_nav2',
										'container'       => false,
										'items_wrap'      => '%3$s',
										'depth'           => 0,
										'walker'          => new FlatsomeNavDropdown
									));
								?>
								<?php if (!isset($flatsome_opt['search_pos']) || $flatsome_opt['search_pos'] == 'left') { ?>
								<li class="search-dropdown">
									<a href="#" class="nav-top-link icon-search" onClick="return false;"></a>
									<div class="nav-dropdown">
										<?php if(function_exists('get_product_search_form')) {
											get_product_search_form();
										} else {
											get_search_form();
										} ?>	
									</div><!-- .nav-dropdown -->
								</li><!-- .search-dropdown -->
								<?php } ?>
								

		                        <?php else: ?>
		                            <li>Define your main navigation in <b>Apperance > Menus</b></li>
		                        <?php endif; ?>								
							</ul>
						<?php } ?>
					</div><!-- .left-links -->
			</div><!-- .large-12 -->
		</div><!-- .row -->
	</header><!-- .header -->
</div><!-- .sticky-wrapper -->
<script>
	function show_search_form(){
		jQuery('#aspk_sidebar123').toggle();
	}
	jQuery( document ).ready(function() {
		jQuery(window).scroll(function () {
		   scroll = jQuery(window).scrollTop();
		  if(scroll >= 200){
			  jQuery('.aspk_nav_menu_scrol').show();
		  }else{
			 jQuery('.aspk_nav_menu_scrol').hide();
		  }
		});
		
		jQuery('#yith-searchsubmit').html('search');
	});
				function show_jewellery(){
					var nav = jQuery('.main_nav_sub212').css('display');
					if(nav=='none'){
						jQuery('.main_nav_sub212').show();
						jQuery('.jewelry').show();
						jQuery('.watches').hide();
						jQuery('.Accessories').hide();
						jQuery('.fragrances').hide();
						jQuery('.signs').hide();
						jQuery('.the-maison-2').hide();
					}else{
						jQuery('.main_nav_sub212').toggle();
						jQuery('.jewelry').show();
						jQuery('.watches').hide();
						jQuery('.Accessories').hide();
						jQuery('.fragrances').hide();
						jQuery('.signs').hide();
						jQuery('.the-maison-2').hide();
					}
				}
				function show_watch(){
					var nav = jQuery('.main_nav_sub212').css('display');
					if(nav=='none'){
						jQuery('.main_nav_sub212').show();
						jQuery('.watches').show();
						jQuery('.jewelry').hide();
						jQuery('.Accessories').hide();
						jQuery('.fragrances').hide();
						jQuery('.signs').hide();
						jQuery('.the-maison-2').hide();
					}else{
						jQuery('.main_nav_sub212').toggle();
						jQuery('.watches').show();
						jQuery('.jewelry').hide();
						jQuery('.Accessories').hide();
						jQuery('.fragrances').hide();
						jQuery('.signs').hide();
						jQuery('.the-maison-2').hide();
					}
				}
				function show_accessories(){
					var nav = jQuery('.main_nav_sub212').css('display');
					if(nav=='none'){
						jQuery('.main_nav_sub212').show();
						jQuery('.watches').hide();
						jQuery('.jewelry').hide();
						jQuery('.Accessories').show();
						jQuery('.fragrances').hide();
						jQuery('.signs').hide();
						jQuery('.the-maison-2').hide();
					}else{
						jQuery('.main_nav_sub212').toggle();
						jQuery('.watches').hide();
						jQuery('.jewelry').hide();
						jQuery('.Accessories').show();
						jQuery('.fragrances').hide();
						jQuery('.signs').hide();
						jQuery('.the-maison-2').hide();
					}
				}
				function show_fragrances(){
					var nav = jQuery('.main_nav_sub212').css('display');
					if(nav=='none'){
						jQuery('.main_nav_sub212').show();
						jQuery('.watches').hide();
						jQuery('.jewelry').hide();
						jQuery('.Accessories').hide();
						jQuery('.fragrances').show();
						jQuery('.signs').hide();
						jQuery('.the-maison-2').hide();
					}else{
						jQuery('.main_nav_sub212').toggle();
						jQuery('.watches').hide();
						jQuery('.jewelry').hide();
						jQuery('.Accessories').hide();
						jQuery('.fragrances').show();
						jQuery('.signs').hide();
						jQuery('.the-maison-2').hide();
					}
				}
				function show_icon_sing(){
					var nav = jQuery('.main_nav_sub212').css('display');
					if(nav=='none'){
						jQuery('.main_nav_sub212').show();
						jQuery('.jewelry').hide();
						jQuery('.watches').hide();
						jQuery('.Accessories').hide();
						jQuery('.fragrances').hide();
						jQuery('.signs').show();
						jQuery('.the-maison-2').hide();
					}else{
						jQuery('.main_nav_sub212').toggle();
						jQuery('.jewelry').hide();
						jQuery('.watches').hide();
						jQuery('.Accessories').hide();
						jQuery('.fragrances').hide();
						jQuery('.signs').show();
						jQuery('.the-maison-2').hide();
					}
				}
				function show_maison(){
					var nav = jQuery('.main_nav_sub212').css('display');
					if(nav=='none'){
						jQuery('.main_nav_sub212').show();
						jQuery('.jewelry').hide();
						jQuery('.watches').hide();
						jQuery('.Accessories').hide();
						jQuery('.fragrances').hide();
						jQuery('.signs').hide();
						jQuery('.the-maison-2').show();
					}else{
						jQuery('.main_nav_sub212').toggle();
						jQuery('.jewelry').hide();
						jQuery('.watches').hide();
						jQuery('.Accessories').hide();
						jQuery('.fragrances').hide();
						jQuery('.signs').hide();
						jQuery('.the-maison-2').show();
					}
				}
</script>

<?php if($flatsome_opt['nav_position'] == 'bottom' || $flatsome_opt['nav_position'] == 'bottom_center') { ?>
<!-- Main navigation - Full width style -->
<div class="wide-nav <?php echo $flatsome_opt['nav_position_color']; ?> <?php if($flatsome_opt['nav_position'] == 'bottom_center') {echo 'nav-center';} else {echo 'nav-left';} ?>">
	<div class="row">
		<div class="large-12 columns">
		<div class="nav-wrapper">
		<ul id="site-navigation" class="header-nav">
				<?php if ( has_nav_menu( 'primary' ) ) : ?>
					<?php  
					wp_nav_menu(array(
						'theme_location' => 'primary',
						'container'       => false,
						'items_wrap'      => '%3$s',
						'depth'           => 3,
						'walker'          => new FlatsomeNavDropdown
					));
				?>

				<?php if($flatsome_opt['search_pos'] == 'right' && $flatsome_opt['nav_position'] == 'bottom_center'){ ?>
					<li class="search-dropdown">
					<a href="#" class="nav-top-link icon-search" onClick="return false;"></a>
					<div class="nav-dropdown">
						<?php if(function_exists('get_product_search_form')) {
							get_product_search_form();
						} else {
							get_search_form();
						} ?>	
					</div><!-- .nav-dropdown -->
				</li><!-- .search-dropdown -->
				<?php } ?>
              <?php else: ?>
                  <li>Define your main navigation in <b>Apperance > Menus</b></li>
              <?php endif; ?>								
		</ul>
		<?php if($flatsome_opt['nav_position'] == 'bottom') { ?>
		<div class="right hide-for-small">
			<div class="wide-nav-right">
				<div>
				<?php echo do_shortcode($flatsome_opt['nav_position_text']); ?>
			</div>
				<?php if($flatsome_opt['search_pos'] == 'right'){ ?>
							<div>
									<?php if(function_exists('get_product_search_form')) {
											get_product_search_form();
										} else {
											get_search_form();
										} ?>		
							</div>
				<?php } ?>
			</div>
		</div>
		<?php } ?>
		</div><!-- nav-wrapper -->
		</div><!-- .large-12 -->
	</div><!-- .row -->
</div><!-- .wide-nav -->
<?php } ?>
</div><!-- .header-wrapper -->


<?php if(isset($flatsome_opt['html_after_header'])){
	// AFTER HEADER HTML BLOCK
	echo do_shortcode($flatsome_opt['html_after_header']);
} ?>

<div id="main-content" class="site-main <?php echo $flatsome_opt['content_color']; ?>">
<?php 
//adds a border line if header is white
if (strpos($flatsome_opt['header_bg'],'#fff') !== false || $flatsome_opt['nav_position'] == 'top') {
		  echo '<div class="row"><div class="large-12 columns"><div class="top-divider"></div></div></div>';
} ?>

<!-- woocommerce message -->
<?php  if(function_exists('wc_print_notices')) {wc_print_notices();}?>